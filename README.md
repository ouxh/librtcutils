Google 开源项目WebRTC中有大量优秀代码，但是WebRTC项目代码量大且编译过程繁琐，引用其基础组件难度较大，为此我将逐步拆分基础组件单独编译，方便学习、复用和研究。

在 Windows, MacOS/iOS, Linux 编译通过

当前组件:

1. TaskQueue

2. MessageQueue